SRC_DIR=src
BIN_DIR=bin

CC=gcc
CFLAGS=`pkg-config fuse --cflags` -I$(SRC_DIR) -Wall -Wextra -Wformat
LIBS=-lm -lsodium `pkg-config fuse --libs`

MODULES=fusencrypt filesystem crypto debug
HEADERS=constants

DEPS=$(patsubst %,$(SRC_DIR)/%.h,$(MODULES) $(HEADERS))
OBJ=$(patsubst %,$(BIN_DIR)/%.o,$(MODULES))

NAME=fusencrypt

all: $(NAME)
$(NAME): $(BIN_DIR)/$(NAME)
$(BIN_DIR)/$(NAME): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

$(BIN_DIR)/%.o: $(SRC_DIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -f $(BIN_DIR)/*

debug:
	make "BUILD=debug"

release:
	make "BUILD=release"

ifeq ($(BUILD),debug)
CFLAGS += -O0 -g -DDEBUG
else ifeq ($(BUILD),release)
CFLAGS += -O2 -s
endif
