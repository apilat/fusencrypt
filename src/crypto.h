#ifndef FUSENCRYPT_CRYPTO
#define FUSENCRYPT_CRYPTO

#include "constants.h"
#include "filesystem.h"

#include <stdio.h>

void memzero(void *ptr, size_t len);
ssize_t readpass(char **ptr, size_t *len, FILE *stream);

int filesystem_init_file_header(struct fusencrypt_file_header *header);
int filesystem_crypto_initp(struct fusencrypt_crypto *crypto, char *pwd, size_t pwdlen, unsigned char key[KEYSIZE]);
int filesystem_crypto_initk(struct fusencrypt_crypto *crypto);
int filesystem_crypto_password_keygen(struct fusencrypt_crypto *crypto, char *pwd, size_t pwdlen,
                                      unsigned char key[KEYSIZE]);

int filesystem_load_crypto(struct fusencrypt_filesystem *fs);
int filesystem_load_superblock(struct fusencrypt_filesystem *fs);
int filesystem_load_file_header(struct fusencrypt_filesystem *fs);

int filesystem_write_crypto(struct fusencrypt_filesystem *fs);
int filesystem_write_superblock(struct fusencrypt_filesystem *fs);
int filesystem_write_file_header(struct fusencrypt_filesystem *fs);

// Important: The buffer for all of these operations needs to have size BLOCKSIZE, *not* just ACTUAL_BLOCKSIZE
int filesystem_write_empty_block(struct fusencrypt_filesystem *fs, block_t blocknum);
int filesystem_write_block(struct fusencrypt_filesystem *fs, void *buf, block_t blocknum);
int filesystem_write_block_copy(struct fusencrypt_filesystem *fs, void *buf, block_t blocknum);
int filesystem_read_block(struct fusencrypt_filesystem *fs, void *buf, block_t blocknum);

int encrypted_write(unsigned char key[KEYSIZE], int fd, void *buf, ssize_t len, unsigned char nonce[NONCESIZE],
                    unsigned char *additional_data, size_t addlen, size_t blocklen, uint64_t offset);
int encrypted_read(unsigned char key[KEYSIZE], int fd, void *buf, ssize_t len, unsigned char nonce[NONCESIZE],
                   unsigned char *additional_data, size_t addlen, size_t blocklen, uint64_t offset);

#endif
