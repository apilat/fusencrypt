#include "filesystem.h"
#include "crypto.h"
#include "debug.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>


static void *filesystem_load_block(struct fusencrypt_filesystem *fs, block_t n);
static void filesystem_unload_block(struct fusencrypt_filesystem *fs, void *ptr);
static int filesystem_allocate_blocks(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data,
                                      uint64_t blocks);
static int filesystem_deallocate_blocks(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data,
                                        uint64_t blocks);
static int filesystem_deallocate_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data);

static int filesystem_load_dir_data(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data *data);
static int filesystem_unload_dir_data(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data);
static int filesystem_rewrite_data_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data);
static int filesystem_fill_dir_data(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data, mode_t mode,
                                    const char *name);
static struct fusencrypt_dir_data fusencrypt_get_root_data(struct fusencrypt_filesystem *fs);

static int is_file(struct fusencrypt_dir_data data);
static int is_dir(struct fusencrypt_dir_data data);

int filesystem_init(struct fusencrypt_filesystem *fs, struct fusencrypt_superblock *sblock,
                    struct fusencrypt_crypto *crypto, struct fusencrypt_file_header *header) {
    debug_tracecall();
    fs->f_crypto = crypto;
    fs->f_superblock = sblock;
    fs->f_header = header;

    fs->f_buffer = sodium_malloc(FSBUFFERSIZE);
    if (fs->f_buffer == NULL)
        return 1;
    fs->f_buffer_size = FSBUFFERSIZE;
    sodium_memzero(fs->f_buffer, FSBUFFERSIZE);

    return 0;
}

int filesystem_superblock_init(struct fusencrypt_filesystem *fs, char *name, uint64_t size) {
    debug_print("%s %lu", name, size);
    if (strlen(name) > SUPERBLOCK_LABELLENMAX) {
        errno = ENAMETOOLONG;
        return 1;
    }

    struct fusencrypt_superblock *sb = fs->f_superblock;

    randombytes_buf(sb->s_uuid, SUPERBLOCK_UUIDLEN);
    strcpy(sb->s_label, name);
    sb->s_magic = SUPERBLOCK_MAGIC;

    sb->s_used_blocks_first_block = 1;
    sb->s_used_blocks_block_count = size / (8 * ACTUAL_BLOCKSIZE) + 1;
    for (block_t b = sb->s_used_blocks_first_block; b < sb->s_used_blocks_first_block + sb->s_used_blocks_block_count;
         b++)
        filesystem_write_empty_block(fs, b);

    sb->s_block_count = size;
    sb->s_free_block_count = size - 1 - sb->s_used_blocks_block_count;
    // TODO add redundant superblock copy

    sb->s_first_data_block = sb->s_used_blocks_first_block + sb->s_used_blocks_block_count;

    if (filesystem_fill_dir_data(fs, fusencrypt_get_root_data(fs), 0755 | S_IFDIR, "/"))
        return 1;

    return 0;
}

static block_t find_empty_block(struct fusencrypt_filesystem *fs) {
    debug_tracecall();
    if (fs->f_superblock->s_free_block_count == 0) {
        debug_print("%s", "no free blocks");
        return 0;
    }
    block_t first = fs->f_superblock->s_used_blocks_first_block;
    block_t last = first + fs->f_superblock->s_used_blocks_block_count;
    uint8_t *bitmap = NULL;
    for (block_t bitmap_block = first; bitmap_block <= last; bitmap_block++) {
        if (bitmap != NULL) {
            filesystem_unload_block(fs, bitmap);
        }

        bitmap = filesystem_load_block(fs, bitmap_block);
        if (bitmap == NULL) {
            return 0;
        }

        for (size_t byte = 0; byte < ACTUAL_BLOCKSIZE; byte++) {
            if (bitmap[byte] == 0xff)
                continue;
            for (size_t bit = 0; bit < 8; bit++) {
                block_t b = (bitmap_block - first) * ACTUAL_BLOCKSIZE + byte * 8 + bit;

                if (b < fs->f_superblock->s_first_data_block) {
                    continue;
                }
                if (b >= fs->f_superblock->s_block_count - 1) {
                    debug_print("%s", "all blocks used");
                    filesystem_unload_block(fs, bitmap);
                    return 0;
                }

                if (!(bitmap[byte] & (1 << bit))) {
                    debug_print("%lu", b);
                    filesystem_unload_block(fs, bitmap);
                    return b;
                }
            }
        }
    }
    abort();
}

static int mark_block_usage(struct fusencrypt_filesystem *fs, block_t b, int used) {
    size_t block_offset = b / 8 / ACTUAL_BLOCKSIZE;
    size_t byte_offset = (b / 8) % ACTUAL_BLOCKSIZE;
    size_t bit_offset = (b % 8) % ACTUAL_BLOCKSIZE;
    block_t block = fs->f_superblock->s_used_blocks_first_block + block_offset;

    uint8_t *bitmap = filesystem_load_block(fs, block);
    if (bitmap == NULL) {
        return 1;
    }

    if (used) {
        bitmap[byte_offset] |= (1 << bit_offset);
    } else {
        bitmap[byte_offset] &= ~(1 << bit_offset);
    }

    filesystem_write_block(fs, bitmap, block);
    filesystem_unload_block(fs, bitmap);
    return 0;
}

static block_t allocate_index_block(struct fusencrypt_filesystem *fs) {
    debug_tracecall();
    block_t b;
    if ((b = find_empty_block(fs)) == 0)
        return 0;

    if (filesystem_write_empty_block(fs, b))
        return 0;

    if (mark_block_usage(fs, b, 1))
        return 0;
    fs->f_superblock->s_free_block_count--;

    if (filesystem_write_superblock(fs))
        return 0;

    debug_print("%lu", b);
    return b;
}

static int add_block_to_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data, block_t b) {
    const unsigned direct = sizeof(data.entry->f_direct_block) / sizeof(data.entry->f_direct_block[0]);
    const unsigned int indirect = sizeof(((struct fusencrypt_index_block *) 0)->i_blocks) /
                                  sizeof(((struct fusencrypt_index_block *) 0)->i_blocks[0]);
    struct fusencrypt_dir_entry *entry = data.entry;
    trace_print("%p %lu", entry, b);

    for (size_t db = 0; db < direct; db++) {
        if (entry->f_direct_block[db] == 0) {
            entry->f_direct_block[db] = b;
            return 0;
        }
    }

    if (entry->f_index_block == 0) {
        entry->f_index_block = allocate_index_block(fs);
        if (entry->f_index_block == 0)
            return 1;
    }
    struct fusencrypt_index_block *indexb = filesystem_load_block(fs, entry->f_index_block);
    if (indexb == NULL) {
        return 1;
    }
    for (size_t db = 0; db < indirect; db++) {
        if (indexb->i_blocks[db] == 0) {
            indexb->i_blocks[db] = b;
            filesystem_write_block(fs, indexb, entry->f_index_block);
            filesystem_unload_block(fs, indexb);
            return 0;
        }
    }
    filesystem_unload_block(fs, indexb);

    if (entry->f_double_index_block == 0) {
        entry->f_double_index_block = allocate_index_block(fs);
        if (entry->f_double_index_block == 0)
            return 1;
    }
    struct fusencrypt_index_block *dindexb = filesystem_load_block(fs, entry->f_double_index_block);
    if (dindexb == NULL) {
        return 1;
    }
    for (size_t ddb = 0; ddb < indirect; ddb++) {
        if (dindexb->i_blocks[ddb] == 0) {
            dindexb->i_blocks[ddb] = allocate_index_block(fs);
            if (dindexb->i_blocks[ddb] == 0) {
                filesystem_unload_block(fs, dindexb);
                return 1;
            }
        }
        struct fusencrypt_index_block *indexb = filesystem_load_block(fs, dindexb->i_blocks[ddb]);
        if (indexb == NULL) {
            filesystem_unload_block(fs, dindexb);
            return 1;
        }
        for (size_t db = 0; db < indirect; db++) {
            if (indexb->i_blocks[db] == 0) {
                indexb->i_blocks[db] = b;
                filesystem_write_block(fs, indexb, dindexb->i_blocks[ddb]);
                filesystem_unload_block(fs, indexb);
                filesystem_write_block(fs, dindexb, entry->f_double_index_block);
                filesystem_unload_block(fs, dindexb);
                return 0;
            }
        }
        filesystem_unload_block(fs, indexb);
    }
    filesystem_unload_block(fs, dindexb);

    if (entry->f_triple_index_block == 0) {
        entry->f_triple_index_block = allocate_index_block(fs);
        if (entry->f_triple_index_block == 0)
            return 1;
    }
    struct fusencrypt_index_block *tindexb = filesystem_load_block(fs, entry->f_triple_index_block);
    if (tindexb == NULL) {
        return 1;
    }
    for (size_t dddb = 0; dddb < indirect; dddb++) {
        if (tindexb->i_blocks[dddb] == 0) {
            tindexb->i_blocks[dddb] = allocate_index_block(fs);
            if (tindexb->i_blocks[dddb] == 0) {
                filesystem_unload_block(fs, tindexb);
                return 1;
            }
        }
        struct fusencrypt_index_block *dindexb = filesystem_load_block(fs, tindexb->i_blocks[dddb]);
        if (dindexb == NULL) {
            filesystem_unload_block(fs, tindexb);
            return 1;
        }
        for (size_t ddb = 0; ddb < indirect; ddb++) {
            if (dindexb->i_blocks[ddb] == 0) {
                dindexb->i_blocks[ddb] = allocate_index_block(fs);
                if (dindexb->i_blocks[ddb] == 0) {
                    filesystem_unload_block(fs, dindexb);
                    filesystem_unload_block(fs, tindexb);
                    return 1;
                }
            }
            struct fusencrypt_index_block *indexb = filesystem_load_block(fs, dindexb->i_blocks[ddb]);
            if (indexb == NULL) {
                filesystem_unload_block(fs, dindexb);
                filesystem_unload_block(fs, tindexb);
                return 1;
            }
            for (size_t db = 0; db < indirect; db++) {
                if (indexb->i_blocks[db] == 0) {
                    indexb->i_blocks[db] = b;
                    filesystem_write_block(fs, indexb, dindexb->i_blocks[ddb]);
                    filesystem_unload_block(fs, indexb);
                    filesystem_write_block(fs, dindexb, tindexb->i_blocks[dddb]);
                    filesystem_unload_block(fs, dindexb);
                    filesystem_write_block(fs, tindexb, entry->f_triple_index_block);
                    filesystem_unload_block(fs, tindexb);
                    return 0;
                }
            }
            filesystem_unload_block(fs, indexb);
        }
        filesystem_unload_block(fs, dindexb);
    }
    filesystem_unload_block(fs, tindexb);

    return 1;
}

static int filesystem_allocate_blocks(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data,
                                      uint64_t blocks) {
    block_t b;
    debug_print("%lu:%u:%p %lu", data.blocknum, data.entrynum, data.entry, blocks);

    while (blocks > 0 && (b = find_empty_block(fs)) != 0) {
        // Need to make sure the block is marked as used so it is not used twice if an index block needs to be allocated
        // for the new entry.
        if (mark_block_usage(fs, b, 1))
            return 1;
        if (add_block_to_entry(fs, data, b)) {
            if (mark_block_usage(fs, b, 0))
                return 1;
            break;
        }

        blocks--;
        data.entry->f_allocated_blocks++;
        fs->f_superblock->s_free_block_count--;

        if (filesystem_write_empty_block(fs, b))
            return 1;
    }

    if (filesystem_write_superblock(fs))
        return 1;

    if (blocks > 0) {
        errno = ENOSPC;
        return 1;
    }
    return 0;
}

static block_t remove_last_block_from_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data) {
    const unsigned direct = sizeof(data.entry->f_direct_block) / sizeof(data.entry->f_direct_block[0]);
    const unsigned int indirect = sizeof(((struct fusencrypt_index_block *) 0)->i_blocks) /
                                  sizeof(((struct fusencrypt_index_block *) 0)->i_blocks[0]);
    struct fusencrypt_dir_entry *entry = data.entry;
    trace_print("%lu:%u:%p", data.blocknum, data.entrynum, data.entry);

    // TODO Correctly handle error if filesystem_load_block returns NULL
    if (entry->f_allocated_blocks > indirect * indirect + indirect + direct && entry->f_triple_index_block != 0) {
        struct fusencrypt_index_block *tindexb = filesystem_load_block(fs, entry->f_triple_index_block);
        if (tindexb != NULL) {
            for (size_t dddb = indirect; dddb > 0; dddb--) {
                struct fusencrypt_index_block *dindexb = filesystem_load_block(fs, tindexb->i_blocks[dddb - 1]);
                if (dindexb != NULL) {
                    for (size_t ddb = indirect; ddb > 0; ddb--) {
                        if (dindexb->i_blocks[ddb - 1] != 0) {
                            struct fusencrypt_index_block *indexb =
                                filesystem_load_block(fs, dindexb->i_blocks[ddb - 1]);
                            if (indexb != NULL) {
                                for (size_t db = indirect; db > 0; db--) {
                                    block_t block = indexb->i_blocks[db - 1];
                                    if (block == 0)
                                        continue;
                                    indexb->i_blocks[db - 1] = 0;
                                    filesystem_write_block(fs, indexb, dindexb->i_blocks[ddb - 1]);
                                    filesystem_unload_block(fs, indexb);
                                    filesystem_unload_block(fs, dindexb);
                                    filesystem_unload_block(fs, tindexb);
                                    return block;
                                }
                                filesystem_unload_block(fs, indexb);
                                dindexb->i_blocks[ddb - 1] = 0;
                                filesystem_write_block_copy(fs, dindexb, tindexb->i_blocks[dddb - 1]);
                            }
                        }
                    }
                    filesystem_unload_block(fs, dindexb);
                    tindexb->i_blocks[dddb - 1] = 0;
                    filesystem_write_block_copy(fs, tindexb, entry->f_triple_index_block);
                }
            }
            filesystem_unload_block(fs, tindexb);
            entry->f_triple_index_block = 0;
        }
    }

    if (entry->f_allocated_blocks > indirect + direct && entry->f_double_index_block != 0) {
        struct fusencrypt_index_block *dindexb = filesystem_load_block(fs, entry->f_double_index_block);
        if (dindexb != NULL) {
            for (size_t ddb = indirect; ddb > 0; ddb--) {
                if (dindexb->i_blocks[ddb - 1] != 0) {
                    struct fusencrypt_index_block *indexb = filesystem_load_block(fs, dindexb->i_blocks[ddb - 1]);
                    if (indexb != NULL) {
                        for (size_t db = indirect; db > 0; db--) {
                            block_t block = indexb->i_blocks[db - 1];
                            if (block == 0)
                                continue;
                            indexb->i_blocks[db - 1] = 0;
                            filesystem_write_block(fs, indexb, dindexb->i_blocks[ddb - 1]);
                            filesystem_unload_block(fs, indexb);
                            filesystem_unload_block(fs, dindexb);
                            return block;
                        }
                        filesystem_unload_block(fs, indexb);
                        dindexb->i_blocks[ddb - 1] = 0;
                        filesystem_write_block_copy(fs, dindexb, entry->f_double_index_block);
                    }
                }
            }
            filesystem_unload_block(fs, dindexb);
            entry->f_double_index_block = 0;
        }
    }

    if (entry->f_allocated_blocks > direct && entry->f_index_block != 0) {
        struct fusencrypt_index_block *indexb = filesystem_load_block(fs, entry->f_index_block);
        if (indexb != NULL) {
            for (size_t db = indirect; db > 0; db--) {
                block_t block = indexb->i_blocks[db - 1];
                if (block == 0)
                    continue;
                indexb->i_blocks[db - 1] = 0;
                filesystem_write_block(fs, indexb, entry->f_index_block);
                filesystem_unload_block(fs, indexb);
                return block;
            }
            filesystem_unload_block(fs, indexb);
            entry->f_index_block = 0;
        }
    }

    for (size_t db = direct; db > 0; db--) {
        if (entry->f_direct_block[db - 1] != 0) {
            block_t block = entry->f_direct_block[db - 1];
            entry->f_direct_block[db - 1] = 0;
            return block;
        }
    }
    return 0;
}

static int filesystem_deallocate_blocks(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data,
                                        uint64_t blocks) {
    block_t b;
    debug_print("%lu:%u:%p %lu", data.blocknum, data.entrynum, data.entry, blocks);

    while (blocks > 0 && (b = remove_last_block_from_entry(fs, data)) != 0) {
        if (mark_block_usage(fs, b, 0))
            return 1;
        blocks--;
        data.entry->f_allocated_blocks--;
        fs->f_superblock->s_free_block_count++;

        if (filesystem_write_empty_block(fs, b))
            return 1;
    }

    if (filesystem_write_superblock(fs))
        return 1;

    if (blocks > 0)
        return 1;

    return 0;
}

static int filesystem_deallocate_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data) {
    debug_print("%lu:%u:%p", data.blocknum, data.entrynum, data.entry);
    if (filesystem_deallocate_blocks(fs, data, data.entry->f_allocated_blocks))
        return 1;
    memzero(data.entry, sizeof(*data.entry));
    int retval = filesystem_rewrite_data_entry(fs, data);
    retval |= filesystem_unload_dir_data(fs, data);
    return retval;
}

static int filesystem_rewrite_data_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data) {
    debug_print("%lu:%u:%p", data.blocknum, data.entrynum, data.entry);
    if (data.blocknum == 0) {
        return filesystem_write_superblock(fs);
    } else {
        return filesystem_write_block(fs, data.block, data.blocknum);
    }
}

static int filesystem_load_dir_data(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data *data) {
    trace_print("%lu:%u:%p", data->blocknum, data->entrynum, data->entry);
    if (data->blocknum == 0)
        return 0;

    data->block = filesystem_load_block(fs, data->blocknum);
    if (data->block == NULL)
        return 1;
    data->entry = &((struct fusencrypt_directory_block *) data->block)->d_entries[data->entrynum];
    return 0;
}

static int filesystem_unload_dir_data(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data) {
    trace_print("%lu:%u:%p", data.blocknum, data.entrynum, data.entry);
    if (data.blocknum != 0) {
        filesystem_unload_block(fs, data.block);
    }
    return 0;
}

static int filesystem_fill_dir_data(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data, mode_t mode,
                                    const char *name) {
    debug_print("%lu:%u:%p %d %s", data.blocknum, data.entrynum, data.entry, mode, name);
    struct fusencrypt_dir_entry *entry = data.entry;
    memzero(entry, sizeof(*entry));
    entry->f_mode = mode;
    entry->f_allocated_blocks = 0;
    strncpy(entry->f_name, name, FILE_NAMELENMAX);
    timespec_get(&entry->f_mtime, TIME_UTC);
    timespec_get(&entry->f_ctime, TIME_UTC);
    if (mode & S_IFDIR) {
        entry->d_magic = DIRECTORY_MAGIC;
        if (filesystem_allocate_blocks(fs, data, 1))
            return 1;
    }
    if (mode & S_IFREG) {
        entry->d_magic = FILE_MAGIC;
    }
    if (mode == TEMP_MAGIC) {
        entry->d_magic = TEMP_MAGIC;
    }
    return 0;
}

static int is_file(struct fusencrypt_dir_data data) {
    return (data.entry != NULL) && (data.entry->f_mode & S_IFREG) && (data.entry->d_magic == FILE_MAGIC);
}

static int is_dir(struct fusencrypt_dir_data data) {
    return (data.entry != NULL) && (data.entry->f_mode & S_IFDIR) && (data.entry->d_magic == DIRECTORY_MAGIC);
}

int filesystem_entry_is_valid(struct fusencrypt_dir_data data) {
    return (data.entry != NULL) && (data.entry->d_magic == DIRECTORY_MAGIC || data.entry->d_magic == FILE_MAGIC ||
                                    data.entry->d_magic == TEMP_MAGIC);
}

struct loaded_block {
    char loaded;
    uint8_t data[BLOCKSIZE];
};

// Helped with debugging since otherwise valgrind is not able to detect out-of-bounds operations in a single huge
// buffer.
//#define LOAD_BLOCK_ALLOC
static void *filesystem_load_block(struct fusencrypt_filesystem *fs, block_t b) {
#ifdef LOAD_BLOCK_ALLOC
    void *ptr = malloc(BLOCKSIZE);
    size_t i = -1;
#else
    struct loaded_block *buffer = (struct loaded_block *) fs->f_buffer;
    const size_t bufsize = fs->f_buffer_size / sizeof(*buffer);
    void *ptr = NULL;
    // previous_i helps debug allocation-related errors since blocks are not loaded on the same addresses
    static int previous_i = 0;
    size_t i = -1;

    for (size_t o = 1; o <= bufsize; o++) {
        i = (previous_i + o) % bufsize;
        if (!buffer[i].loaded) {
            buffer[i].loaded = 1;
            ptr = &buffer[i].data;
            previous_i = i;
            break;
        }
    }

    if (ptr == NULL) {
        errno = EBUSY;
        error_print("No buffer slots available! Cannot load block %lu", b);
        return NULL;
    }
#endif

    switch (filesystem_read_block(fs, ptr, b)) {
    case 2:
        errno = EACCES;
        __attribute__((fallthrough));
    case 1:
#ifdef LOAD_BLOCK_ALLOC
        free(ptr);
#else
        buffer[i].loaded = 0;
#endif
        return NULL;
    }
    trace_print("%lu %lu %p", b, i, ptr);
    return ptr;
}

static void filesystem_unload_block(struct fusencrypt_filesystem *fs, void *ptr) {
#ifdef LOAD_BLOCK_ALLOC
    trace_print("%p", ptr);
    free(ptr);
#else
    struct loaded_block *buffer = (struct loaded_block *) fs->f_buffer;
    const size_t bufsize = fs->f_buffer_size / sizeof(*buffer);

    for (size_t i = 0; i < bufsize; i++) {
        if (buffer[i].loaded && ptr == &buffer[i].data) {
            buffer[i].loaded = 0;
            memzero(&buffer[i].data, sizeof(buffer[i].data));
            trace_print("%lu %p", i, ptr);
            return;
        }
    }
    warn_print("Block at %p is not loaded. Not unloading anything", ptr);
#endif
}

int filesystem_check_buffer_state(struct fusencrypt_filesystem *fs) {
    struct loaded_block *buffer = (struct loaded_block *) fs->f_buffer;
    const size_t bufsize = fs->f_buffer_size / sizeof(*buffer);

    int found_loaded = 0;
    for (size_t i = 0; i < bufsize; i++) {
        if (buffer[i].loaded) {
            warn_print("Block loaded at %p. This could be a memory leak", buffer[i].data);
            found_loaded = 1;
        }
    }

    return found_loaded;
}

struct filesystem_data_block_iterator {
    struct fusencrypt_filesystem *fs;
    struct fusencrypt_dir_data data;
    size_t next_block;
    struct fusencrypt_index_block *index;
    struct fusencrypt_index_block *dindex;
    struct fusencrypt_index_block *tindex;
};

struct filesystem_data_block_iterator *filesystem_foreach_data_block_new(struct fusencrypt_filesystem *fs,
                                                                         struct fusencrypt_dir_data data) {
    trace_print("%lu:%u:%p", data.blocknum, data.entrynum, data.entry);
    struct filesystem_data_block_iterator iter = {fs, data, 0, NULL, NULL, NULL};
    struct filesystem_data_block_iterator *ptr = malloc(sizeof(iter));
    memcpy(ptr, &iter, sizeof(*ptr));
    return ptr;
}

sblock_t filesystem_foreach_data_block_next(struct filesystem_data_block_iterator *iter) {
    trace_print("%lu:%u:%p %ld", iter->data.blocknum, iter->data.entrynum, iter->data.entry, iter->next_block);
    const unsigned int direct = sizeof(iter->data.entry->f_direct_block) / sizeof(iter->data.entry->f_direct_block[0]);
    const unsigned int indirect = sizeof(((struct fusencrypt_index_block *) 0)->i_blocks) /
                                  sizeof(((struct fusencrypt_index_block *) 0)->i_blocks[0]);
    struct fusencrypt_dir_entry *entry = iter->data.entry;
    size_t next = iter->next_block;
    if (next >= entry->f_allocated_blocks) {
        return -1;
    }

    // TODO Don't ignore errors in filesystem_load_block
    if (next < direct) {
        iter->next_block += 1;
        return entry->f_direct_block[next];
    } else if (next < direct + indirect) {
        next -= direct;
        if (iter->index == NULL) {
            if (entry->f_index_block == 0)
                return -1;
            iter->index = filesystem_load_block(iter->fs, entry->f_index_block);
            if (iter->index == NULL)
                return -1;
        }

        iter->next_block += 1;
        return iter->index->i_blocks[next];
    } else if (next < direct + indirect + indirect * indirect) {
        next -= direct + indirect;
        size_t ddb = next / indirect;
        size_t db = next % indirect;
        if (iter->dindex == NULL) {
            if (entry->f_double_index_block == 0)
                return -1;
            iter->dindex = filesystem_load_block(iter->fs, entry->f_double_index_block);
            if (iter->dindex == NULL)
                return -1;
        }

        if (db == 0) {
            if (iter->index != NULL)
                filesystem_unload_block(iter->fs, iter->index);
            iter->index = filesystem_load_block(iter->fs, iter->dindex->i_blocks[ddb]);
            if (iter->index == NULL)
                return -1;
        }

        iter->next_block += 1;
        return iter->index->i_blocks[db];
    } else {
        next -= direct + indirect + indirect * indirect;
        size_t dddb = next / indirect / indirect;
        size_t ddb = (next / indirect) % indirect;
        size_t db = next % indirect;
        if (iter->tindex == NULL) {
            if (entry->f_triple_index_block == 0)
                return -1;
            iter->tindex = filesystem_load_block(iter->fs, entry->f_triple_index_block);
            if (iter->tindex == NULL)
                return -1;
        }

        if (ddb == 0) {
            if (iter->dindex != NULL)
                filesystem_unload_block(iter->fs, iter->dindex);
            iter->dindex = filesystem_load_block(iter->fs, iter->tindex->i_blocks[dddb]);
            if (iter->dindex == NULL)
                return -1;
        }

        if (db == 0) {
            if (iter->index != NULL)
                filesystem_unload_block(iter->fs, iter->index);
            iter->index = filesystem_load_block(iter->fs, iter->dindex->i_blocks[ddb]);
            if (iter->index == NULL)
                return -1;
        }

        iter->next_block += 1;
        return iter->index->i_blocks[db];
    }
}

void filesystem_foreach_data_block_finish(struct filesystem_data_block_iterator *iter) {
    trace_print("%lu:%u:%p", iter->data.blocknum, iter->data.entrynum, iter->data.entry);
    if (iter->index)
        filesystem_unload_block(iter->fs, iter->index);
    if (iter->dindex)
        filesystem_unload_block(iter->fs, iter->dindex);
    if (iter->tindex)
        filesystem_unload_block(iter->fs, iter->tindex);
    free(iter);
}

struct filesystem_dir_entry_iterator {
    struct fusencrypt_filesystem *fs;
    struct filesystem_data_block_iterator *iter;
    struct fusencrypt_directory_block *db;
    block_t blocknum;
    ssize_t entrynum;
};

struct filesystem_dir_entry_iterator *filesystem_foreach_dir_entry_new(struct fusencrypt_filesystem *fs,
                                                                       struct fusencrypt_dir_data data) {
    trace_print("%lu:%u:%p", data.blocknum, data.entrynum, data.entry);
    struct filesystem_dir_entry_iterator *ptr = malloc(sizeof(*ptr));
    ptr->fs = fs;
    ptr->iter = filesystem_foreach_data_block_new(fs, data);
    ptr->db = NULL;
    ptr->blocknum = 0;
    ptr->entrynum = -1;
    return ptr;
}

struct fusencrypt_dir_data filesystem_foreach_dir_entry_next(struct filesystem_dir_entry_iterator *iter) {
    trace_print("%lu:%u:%p %lu:%zd", iter->iter->data.blocknum, iter->iter->data.entrynum, iter->iter->data.entry,
                iter->blocknum, iter->entrynum);
    const unsigned int entries = sizeof(iter->db->d_entries) / sizeof(iter->db->d_entries[0]);
    const struct fusencrypt_dir_data empty = {0};

    // TODO Don't silently ignore errors in load_block
    if (iter->entrynum == -1 || iter->entrynum == entries) {
        sblock_t block = filesystem_foreach_data_block_next(iter->iter);
        if (block == -1)
            return empty;
        iter->blocknum = block;
        iter->entrynum = 0;
        if (iter->db != NULL)
            filesystem_unload_block(iter->fs, iter->db);
        iter->db = filesystem_load_block(iter->fs, block);
        if (iter->db == NULL)
            return empty;
    }

    struct fusencrypt_dir_data entry;
    entry.entry = &iter->db->d_entries[iter->entrynum];
    entry.block = iter->db;
    entry.entrynum = iter->entrynum;
    entry.blocknum = iter->blocknum;
    iter->entrynum += 1;
    return entry;
}

void filesystem_foreach_dir_entry_finish(struct filesystem_dir_entry_iterator *iter) {
    trace_print("%lu:%u:%p", iter->iter->data.blocknum, iter->iter->data.entrynum, iter->iter->data.entry);
    filesystem_foreach_data_block_finish(iter->iter);
    if (iter->db != NULL)
        filesystem_unload_block(iter->fs, iter->db);
    free(iter);
}

int filesystem_create_dir(struct fusencrypt_filesystem *fs, const char *path, mode_t mode) {
    return filesystem_create_file(fs, path, mode | S_IFDIR);
}

int filesystem_remove_dir(struct fusencrypt_filesystem *fs, const char *path) {
    info_print("%s", path);
    struct fusencrypt_dir_data child = filesystem_get_file(fs, path);

    if (filesystem_load_dir_data(fs, &child))
        return 1;

    if (!filesystem_entry_is_valid(child)) {
        errno = ENOENT;
        goto err;
    }
    if (!is_dir(child)) {
        errno = ENOTDIR;
        goto err;
    }

    struct filesystem_dir_entry_iterator *it = filesystem_foreach_dir_entry_new(fs, child);
    struct fusencrypt_dir_data entry;
    int empty = 1;
    while ((entry = filesystem_foreach_dir_entry_next(it)).entry != NULL) {
        if (filesystem_entry_is_valid(entry)) {
            empty = 0;
            break;
        }
    }
    filesystem_foreach_dir_entry_finish(it);

    if (!empty) {
        errno = ENOTEMPTY;
        goto err;
    }

    if (filesystem_deallocate_entry(fs, child))
        goto err;

    return 0;

err:
    filesystem_unload_dir_data(fs, child);
    return 1;
}

int filesystem_create_file(struct fusencrypt_filesystem *fs, const char *path, mode_t mode) {
    info_print("%s %d", path, mode);
    char *sep = strrchr(path, '/');
    char *parentpath = strndup(path, sep - path);
    const char *childname = sep + 1;
    struct fusencrypt_dir_data parent, child, empty;
    parent.blocknum = child.blocknum = empty.blocknum = 0;
    parent = filesystem_get_file(fs, parentpath);
    free(parentpath);

    if (filesystem_load_dir_data(fs, &parent))
        return 1;
    if (!filesystem_entry_is_valid(parent)) {
        errno = ENOENT;
        goto err;
    }
    if (!is_dir(parent)) {
        errno = ENOTDIR;
        goto err;
    }

    child = filesystem_get_child(fs, parent, childname);
    if (filesystem_load_dir_data(fs, &child))
        goto err;
    if (child.entry != NULL) {
        errno = EEXIST;
        goto err;
    }
    filesystem_unload_dir_data(fs, child);

    empty.entry = NULL;

    struct filesystem_dir_entry_iterator *it = filesystem_foreach_dir_entry_new(fs, parent);
    struct fusencrypt_dir_data entry;
    while ((entry = filesystem_foreach_dir_entry_next(it)).entry != NULL) {
        if (entry.entry != NULL && !filesystem_entry_is_valid(entry)) {
            empty = entry;
            break;
        }
    }
    filesystem_foreach_dir_entry_finish(it);

    if (empty.entry == NULL) {
        if (filesystem_allocate_blocks(fs, parent, 1))
            goto err;

        it = filesystem_foreach_dir_entry_new(fs, parent);
        while ((entry = filesystem_foreach_dir_entry_next(it)).entry != NULL) {
            if (entry.entry != NULL && !filesystem_entry_is_valid(entry)) {
                empty = entry;
                break;
            }
        }
        filesystem_foreach_dir_entry_finish(it);

        if (filesystem_rewrite_data_entry(fs, parent))
            goto err;
        if (empty.entry == NULL)
            goto err;
    }

    filesystem_unload_dir_data(fs, parent);

    if (filesystem_load_dir_data(fs, &empty))
        goto err;
    if (filesystem_fill_dir_data(fs, empty, mode, childname))
        goto err;
    if (filesystem_rewrite_data_entry(fs, empty))
        goto err;
    if (filesystem_unload_dir_data(fs, empty))
        goto err;

    return 0;

err:
    filesystem_unload_dir_data(fs, parent);
    filesystem_unload_dir_data(fs, child);
    filesystem_unload_dir_data(fs, empty);
    return 1;
}

int filesystem_remove_file(struct fusencrypt_filesystem *fs, const char *path) {
    info_print("%s", path);
    struct fusencrypt_dir_data child = filesystem_get_file(fs, path);
    if (filesystem_load_dir_data(fs, &child))
        return 1;
    if (!filesystem_entry_is_valid(child)) {
        errno = ENOENT;
        goto err;
    }
    if (!is_file(child)) {
        errno = EISDIR;
        goto err;
    }

    if (filesystem_deallocate_entry(fs, child))
        goto err;

    return 0;

err:
    filesystem_unload_dir_data(fs, child);
    return 1;
}

ssize_t filesystem_read_file(struct fusencrypt_filesystem *fs, const char *path, uint8_t *buf, size_t len,
                             off_t offset) {
    info_print("%s %p %lu %lu", path, buf, len, offset);
    ssize_t read = 0;
    struct fusencrypt_dir_data file = filesystem_get_file(fs, path);
    if (filesystem_load_dir_data(fs, &file))
        return 0;
    if (!filesystem_entry_is_valid(file)) {
        errno = ENOENT;
        goto end;
    }
    if (!is_file(file)) {
        errno = EISDIR;
        goto end;
    }

    uint64_t filesize = file.entry->f_filesize;
    size_t toread;
    if (len == 0)
        goto end;
    if (offset < 0)
        goto end;
    if ((uint64_t) offset >= filesize)
        goto end;
    toread = filesize - offset;
    if (toread > len)
        toread = len;

    uint64_t relblk = 0;
    struct filesystem_data_block_iterator *it = filesystem_foreach_data_block_new(fs, file);
    sblock_t blocknum;
    while ((blocknum = filesystem_foreach_data_block_next(it)) != -1) {
        if (toread == 0)
            break;
        if (relblk++ < offset / ACTUAL_BLOCKSIZE)
            continue;

        uint64_t offsetblock = (offset / ACTUAL_BLOCKSIZE == relblk - 1) ? offset % ACTUAL_BLOCKSIZE : 0;
        uint64_t maxblockread = ACTUAL_BLOCKSIZE - offsetblock;
        uint64_t toreadblock = (toread > maxblockread) ? maxblockread : toread;
        toread -= toreadblock;

        uint8_t *block = filesystem_load_block(fs, blocknum);
        // TODO Correctly handle error if filesystem_load_block returns NULL
        if (block != NULL) {
            memcpy(buf + read, block + offsetblock, toreadblock);
            read += toreadblock;
            filesystem_unload_block(fs, block);
        }
    }
    filesystem_foreach_data_block_finish(it);
end:
    filesystem_unload_dir_data(fs, file);

    return read;
}

ssize_t filesystem_write_file(struct fusencrypt_filesystem *fs, const char *path, const uint8_t *buf, size_t len,
                              off_t offset) {
    info_print("%s %p %lu %lu", path, buf, len, offset);
    size_t written = 0;
    struct fusencrypt_dir_data file = filesystem_get_file(fs, path);
    if (filesystem_load_dir_data(fs, &file))
        return 0;
    if (!filesystem_entry_is_valid(file)) {
        errno = ENOENT;
        goto end;
    }
    if (!is_file(file)) {
        errno = EISDIR;
        goto end;
    }

    uint64_t filesize = file.entry->f_filesize;
    size_t towrite;
    if (len == 0)
        goto end;
    if (offset < 0)
        goto end;
    if ((uint64_t) offset + len > filesize) {
        uint64_t blknalloc = (offset + len) / ACTUAL_BLOCKSIZE;
        if ((offset + len) % ACTUAL_BLOCKSIZE != 0)
            blknalloc += 1;

        filesize = offset + len;
        file.entry->f_filesize = filesize;

        if (filesystem_allocate_blocks(fs, file, blknalloc - file.entry->f_allocated_blocks))
            goto end;
    }
    towrite = filesize - offset;
    if (towrite > len)
        towrite = len;

    uint64_t relblk = 0;
    struct filesystem_data_block_iterator *it = filesystem_foreach_data_block_new(fs, file);
    sblock_t blocknum;
    while ((blocknum = filesystem_foreach_data_block_next(it)) != -1) {
        if (towrite == 0)
            break;
        if (relblk++ < offset / ACTUAL_BLOCKSIZE)
            continue;

        uint64_t offsetblock = (offset / ACTUAL_BLOCKSIZE == relblk - 1) ? offset % ACTUAL_BLOCKSIZE : 0;
        uint64_t maxblockwrite = ACTUAL_BLOCKSIZE - offsetblock;
        uint64_t towriteblock = (towrite > maxblockwrite) ? maxblockwrite : towrite;
        towrite -= towriteblock;

        uint8_t *block = filesystem_load_block(fs, blocknum);
        // TODO Correctly handle error if filesystem_load_block returns NULL
        if (block != NULL) {
            memcpy(block + offsetblock, buf + written, towriteblock);
            if (!filesystem_write_block(fs, block, blocknum)) {
                written += towriteblock;
            }
            filesystem_unload_block(fs, block);
        }
    }
    filesystem_foreach_data_block_finish(it);

    timespec_get(&file.entry->f_mtime, TIME_UTC);
    timespec_get(&file.entry->f_ctime, TIME_UTC);
    filesystem_rewrite_data_entry(fs, file);
end:
    filesystem_unload_dir_data(fs, file);

    return written;
}

int filesystem_truncate_file(struct fusencrypt_filesystem *fs, const char *path, off_t size) {
    info_print("%s %lu", path, size);
    struct fusencrypt_dir_data file = filesystem_get_file(fs, path);
    if (filesystem_load_dir_data(fs, &file))
        return 1;
    if (!filesystem_entry_is_valid(file)) {
        errno = ENOENT;
        goto err;
    }
    if (!is_file(file)) {
        errno = EISDIR;
        goto err;
    }

    uint64_t filesize = file.entry->f_filesize;
    if (size < 0) {
        errno = EFBIG;
        goto err;
    }
    if (filesize == (uint64_t) size) {
        filesystem_unload_dir_data(fs, file);
        return 0;
    }

    file.entry->f_filesize = size;
    if ((uint64_t) size < filesize) {
        uint64_t blknalloc = size / ACTUAL_BLOCKSIZE;
        uint64_t lbsize = size % ACTUAL_BLOCKSIZE;
        if (lbsize != 0)
            blknalloc += 1;
        if (filesystem_deallocate_blocks(fs, file, file.entry->f_allocated_blocks - blknalloc))
            goto err;
        if (lbsize > 0) {
            sblock_t blocknum;
            block_t last = 0;
            struct filesystem_data_block_iterator *it = filesystem_foreach_data_block_new(fs, file);
            while ((blocknum = filesystem_foreach_data_block_next(it)) != -1) {
                last = blocknum;
            }
            filesystem_foreach_data_block_finish(it);
            uint8_t *block = filesystem_load_block(fs, last);
            if (block == NULL)
                goto err;

            memzero(block + lbsize, ACTUAL_BLOCKSIZE - lbsize);

            filesystem_write_block(fs, block, last);
            filesystem_unload_block(fs, block);
        }
    }

    if ((uint64_t) size > filesize) {
        uint64_t blknalloc = size / ACTUAL_BLOCKSIZE;
        if (size % ACTUAL_BLOCKSIZE != 0)
            blknalloc += 1;
        if (filesystem_allocate_blocks(fs, file, blknalloc - file.entry->f_allocated_blocks))
            goto err;
    }

    timespec_get(&file.entry->f_mtime, TIME_UTC);
    timespec_get(&file.entry->f_ctime, TIME_UTC);

    if (filesystem_rewrite_data_entry(fs, file))
        goto err;

    if (filesystem_unload_dir_data(fs, file))
        return 1;

    return 0;
err:
    filesystem_unload_dir_data(fs, file);
    return 1;
}

int filesystem_utimens(struct fusencrypt_filesystem *fs, const char *path, const struct timespec tv[2]) {
    info_print("%s %ld.%ld", path, tv[1].tv_sec, tv[1].tv_nsec);
    struct fusencrypt_dir_data file = filesystem_get_file(fs, path);
    if (filesystem_load_dir_data(fs, &file))
        return 1;
    if (!filesystem_entry_is_valid(file)) {
        errno = ENOENT;
        goto err;
    }

    memcpy(&file.entry->f_mtime, &tv[1], sizeof(tv[1]));
    timespec_get(&file.entry->f_ctime, TIME_UTC);

    if (filesystem_rewrite_data_entry(fs, file))
        goto err;

    filesystem_unload_dir_data(fs, file);
    return 0;
err:
    filesystem_unload_dir_data(fs, file);
    return 1;
}

int filesystem_rename_file(struct fusencrypt_filesystem *fs, const char *opath, const char *npath) {
    info_print("%s %s", opath, npath);
    struct fusencrypt_dir_data overwrite = {0}, file = {0}, new = {0};

    // overwrite needs to be loaded before file because it may change the underlying dir_entry of opath
    overwrite = filesystem_get_file(fs, npath);
    if (filesystem_load_dir_data(fs, &overwrite))
        goto err;
    if (is_dir(overwrite)) {
        errno = EISDIR;
        goto err;
    }
    if (is_file(overwrite)) {
        if (filesystem_remove_file(fs, npath))
            goto err;
    }
    filesystem_unload_dir_data(fs, overwrite);

    file = filesystem_get_file(fs, opath);
    if (filesystem_load_dir_data(fs, &file))
        return 1;
    if (!filesystem_entry_is_valid(file)) {
        errno = ENOENT;
        goto err;
    }

    char *osep = strrchr(opath, '/');
    char *nsep = strrchr(npath, '/');
    const char *nname = nsep + 1;
    strncpy(file.entry->f_name, nname, FILE_NAMELENMAX);

    if (osep - opath != nsep - npath || strncmp(opath, npath, osep - opath) != 0) {
        // parent paths are not the same
        if (filesystem_create_file(fs, npath, TEMP_MAGIC))
            goto err;

        new = filesystem_get_file(fs, npath);
        if (filesystem_load_dir_data(fs, &new))
            goto err;
        memcpy(new.entry, file.entry, sizeof(struct fusencrypt_dir_entry));
        memzero(file.entry, sizeof(struct fusencrypt_dir_entry));
        if (filesystem_rewrite_data_entry(fs, new))
            goto err;
        filesystem_unload_dir_data(fs, new);
    }

    if (filesystem_rewrite_data_entry(fs, file))
        goto err;

    filesystem_unload_dir_data(fs, file);
    return 0;
err:
    filesystem_unload_dir_data(fs, file);
    filesystem_unload_dir_data(fs, overwrite);
    filesystem_unload_dir_data(fs, new);
    return 1;
}

static struct fusencrypt_dir_data fusencrypt_get_root_data(struct fusencrypt_filesystem *fs) {
    struct fusencrypt_dir_data data;
    data.blocknum = 0;
    data.block = NULL;
    data.entrynum = 0;
    data.entry = &fs->f_superblock->s_root;
    return data;
}

struct fusencrypt_dir_data filesystem_get_file(struct fusencrypt_filesystem *fs, const char *path) {
    debug_print("%s", path);
    struct fusencrypt_dir_data cur = fusencrypt_get_root_data(fs), child = {0};
    char *save;
    char *name = strtok_r((char *) path, "/", &save);

    while (name != NULL) {
        if (*name == '\0') {
            break;
        } else {
            filesystem_load_dir_data(fs, &cur);
            if (!filesystem_entry_is_valid(cur)) {
                filesystem_unload_dir_data(fs, cur);
                break;
            }
            child = filesystem_get_child(fs, cur, name);
            filesystem_unload_dir_data(fs, cur);
            cur = child;
        }
        name = strtok_r(NULL, "/", &save);
    }

    debug_print("%lu:%u:%p", cur.blocknum, cur.entrynum, cur.entry);
    return cur;
}

struct fusencrypt_dir_data filesystem_get_child(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data entry,
                                                const char *name) {
    debug_print("%lu:%u:%p %s", entry.blocknum, entry.entrynum, entry.entry, name);
    struct fusencrypt_dir_data child = {0};
    if (!is_dir(entry)) {
        errno = ENOTDIR;
        return child;
    }

    struct filesystem_dir_entry_iterator *it = filesystem_foreach_dir_entry_new(fs, entry);
    struct fusencrypt_dir_data data;
    while ((data = filesystem_foreach_dir_entry_next(it)).entry != NULL) {
        if (filesystem_entry_is_valid(data) && strcmp(data.entry->f_name, name) == 0) {
            child = data;
            break;
        }
    }
    filesystem_foreach_dir_entry_finish(it);

    debug_print("%lu:%u:%p", child.blocknum, child.entrynum, child.entry);
    return child;
}

void filesystem_cleanup_file(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data) {
    filesystem_unload_dir_data(fs, data);
}

int filesystem_fetch_file(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data *data) {
    return filesystem_load_dir_data(fs, data);
}
