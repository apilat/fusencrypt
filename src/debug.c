#include "debug.h"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

static int _debug_level =
#ifdef DEBUG
    LEVEL_DEBUG
#else
    LEVEL_WARN
#endif
    ;

int debug_level() { return _debug_level; }

int debug_setup() {
    char *var = getenv("DEBUG");
    if (var) {
        if (strcasecmp(var, "trace") == 0)
            _debug_level = LEVEL_TRACE;
        else if (strcasecmp(var, "debug") == 0)
            _debug_level = LEVEL_DEBUG;
        else if (strcasecmp(var, "info") == 0)
            _debug_level = LEVEL_INFO;
        else if (strcasecmp(var, "warn") == 0)
            _debug_level = LEVEL_WARN;
        else if (strcasecmp(var, "error") == 0)
            _debug_level = LEVEL_ERROR;
        else {
            fprintf(stderr, "Unknown debug level: %s\n", var);
            return 1;
        }
        return 0;
    } else {
        return 0;
    }
}
