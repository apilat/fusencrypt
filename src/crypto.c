#include "crypto.h"
#include "debug.h"

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

void memzero(void *ptr, size_t len) { memset(ptr, 0, len); }

static volatile int is_readpass_interrupted;
static void readpass_interrupt(int signum __attribute__((unused))) { is_readpass_interrupted = 1; }

ssize_t readpass(char **ptr, size_t *len, FILE *stream) {
    struct sigaction interrupt, old_interrupt;
    int overwritten_interrupt = 0;
    interrupt.sa_handler = readpass_interrupt;
    sigemptyset(&interrupt.sa_mask);
    interrupt.sa_flags = 0;

    if (sigaction(SIGINT, &interrupt, &old_interrupt)) {
        is_readpass_interrupted = 0;
        fprintf(stderr, "Could not set interrupt handler. Terminal might not be usable after interrupt.\n");
    } else {
        overwritten_interrupt = 1;
    }

    struct termios old, new;
    ssize_t lread;
    int fd = fileno(stream);
    int terminal = 0;

    if (!tcgetattr(fd, &old)) {
        terminal = 1;

        new = old;
        new.c_lflag &= ~ECHO;

        if (tcsetattr(fd, TCSAFLUSH, &new))
            fprintf(stderr,
                    "Could not disable terminal echo.\nYour password will be visible! Press Ctrl-C to abort.\n");
    } else if (errno == ENOTTY) {
        fprintf(stderr, "Standard input is not a terminal; echo is not disabled.\n");
    } else {
        return -1;
    }

    // read will be interrupted on sigint
    lread = getline(ptr, len, stream);
    if (overwritten_interrupt && is_readpass_interrupted) {
        tcsetattr(fd, TCSAFLUSH, &old);
        exit(0);
    }

    if (*(*ptr + lread - 1) == '\n')
        *(*ptr + lread - 1) = '\0';

    if (terminal)
        tcsetattr(fd, TCSAFLUSH, &old);

    if (overwritten_interrupt)
        sigaction(SIGINT, &old_interrupt, NULL);

    return lread;
}

int filesystem_init_file_header(struct fusencrypt_file_header *header) {
    memzero(header, sizeof(*header));

    memcpy(header->header, FILE_HEADER, sizeof(header->header));
    header->version_major = VERSION_MAJOR;
    header->version_minor = VERSION_MINOR;
    header->version_rev = VERSION_REV;

    return 0;
}

int filesystem_crypto_initp(struct fusencrypt_crypto *crypto, char *pwd, size_t pwdlen, unsigned char key[KEYSIZE]) {
    crypto->access_method = 1;

    crypto->argon2_opslimit = ARGON2_OPSLIMIT;
    crypto->argon2_memlimit = ARGON2_MEMLIMIT;
    crypto->argon2_alg = ARGON2_ALG;

    randombytes_buf(crypto->argon2_salt, 16);

    if (filesystem_crypto_password_keygen(crypto, pwd, pwdlen, key))
        return 1;

    randombytes_buf(crypto->s_encryption_nonce, NONCESIZE);

    return 0;
}

int filesystem_crypto_password_keygen(struct fusencrypt_crypto *crypto, char *pwd, size_t pwdlen,
                                      unsigned char key[KEYSIZE]) {
    if (crypto_pwhash_argon2i(key, KEYSIZE, pwd, pwdlen, crypto->argon2_salt, crypto->argon2_opslimit,
                              crypto->argon2_memlimit, crypto->argon2_alg) != 0) {
        memzero(key, KEYSIZE);
        return 1;
    }
    return 0;
}

int filesystem_crypto_initk(struct fusencrypt_crypto *crypto) {
    memzero(crypto, sizeof(*crypto));

    crypto->access_method = 0;

    randombytes_buf(crypto->s_encryption_nonce, NONCESIZE);

    return 0;
}

int filesystem_load_crypto(struct fusencrypt_filesystem *fs) {
    return pread(fs->fd, fs->f_crypto, sizeof(*(fs->f_crypto)), sizeof(struct fusencrypt_file_header)) !=
           sizeof(*(fs->f_crypto));
}

int filesystem_load_file_header(struct fusencrypt_filesystem *fs) {
    return pread(fs->fd, fs->f_header, sizeof(*(fs->f_header)), 0) != sizeof(*(fs->f_header));
}

int filesystem_load_superblock(struct fusencrypt_filesystem *fs) {
    int retval = encrypted_read(fs->f_encryption_key, fs->fd, fs->f_superblock, sizeof(*(fs->f_superblock)),
                                fs->f_crypto->s_encryption_nonce, NULL, 0, BLOCKSIZE - CRYPTO_LEN, CRYPTO_LEN);
    if (retval != 0)
        return retval;
    if (fs->f_superblock->s_magic != SUPERBLOCK_MAGIC)
        return 1;
    return 0;
}

int filesystem_write_crypto(struct fusencrypt_filesystem *fs) {
    return pwrite(fs->fd, fs->f_crypto, sizeof(*(fs->f_crypto)), sizeof(struct fusencrypt_file_header)) !=
           sizeof(*(fs->f_crypto));
}

int filesystem_write_file_header(struct fusencrypt_filesystem *fs) {
    return pwrite(fs->fd, fs->f_header, sizeof(*(fs->f_header)), 0) != sizeof(*(fs->f_header));
}

int filesystem_write_superblock(struct fusencrypt_filesystem *fs) {
    uint8_t buffer[sizeof(*(fs->f_superblock)) + CHECKSUMSIZE];
    memcpy(buffer, fs->f_superblock, sizeof(*(fs->f_superblock)));
    return encrypted_write(fs->f_encryption_key, fs->fd, buffer, sizeof(*(fs->f_superblock)),
                           fs->f_crypto->s_encryption_nonce, NULL, 0, BLOCKSIZE - CRYPTO_LEN, CRYPTO_LEN);
}

int filesystem_write_empty_block(struct fusencrypt_filesystem *fs, block_t blocknum) {
    uint8_t empty[BLOCKSIZE] = {0};
    return filesystem_write_block(fs, empty, blocknum);
}

int filesystem_write_block_copy(struct fusencrypt_filesystem *fs, void *buf, block_t blocknum) {
    uint8_t buffer[BLOCKSIZE];
    memcpy(buffer, buf, ACTUAL_BLOCKSIZE);
    return filesystem_write_block(fs, buffer, blocknum);
}

int filesystem_write_block(struct fusencrypt_filesystem *fs, void *buf, block_t blocknum) {
    uint8_t noncebuffer[NONCESIZE];
    uint8_t blockbuffer[NONCESIZE] = {0};

    memcpy(noncebuffer, fs->f_crypto->s_encryption_nonce, NONCESIZE);
    *((block_t *) blockbuffer) = blocknum;
    sodium_add(noncebuffer, blockbuffer, NONCESIZE);

    return encrypted_write(fs->f_encryption_key, fs->fd, buf, ACTUAL_BLOCKSIZE, noncebuffer, fs->f_superblock->s_uuid,
                           SUPERBLOCK_UUIDLEN, BLOCKSIZE, blocknum * BLOCKSIZE);
}

int filesystem_read_block(struct fusencrypt_filesystem *fs, void *buf, block_t blocknum) {
    uint8_t noncebuffer[NONCESIZE];
    uint8_t blockbuffer[NONCESIZE] = {0};

    memcpy(noncebuffer, fs->f_crypto->s_encryption_nonce, NONCESIZE);
    *((block_t *) blockbuffer) = blocknum;
    sodium_add(noncebuffer, blockbuffer, NONCESIZE);

    return encrypted_read(fs->f_encryption_key, fs->fd, buf, ACTUAL_BLOCKSIZE, noncebuffer, fs->f_superblock->s_uuid,
                          SUPERBLOCK_UUIDLEN, BLOCKSIZE, blocknum * BLOCKSIZE);
}

int encrypted_write(unsigned char key[KEYSIZE], int fd, void *buf, ssize_t len, unsigned char nonce[NONCESIZE],
                    unsigned char *additional_data, size_t addlen, size_t blocklen, uint64_t offset) {
    if (crypto_aead_xchacha20poly1305_ietf_encrypt_detached(buf, buf + len, NULL, buf, len, additional_data, addlen,
                                                            NULL, nonce, key) != 0)
        return 1;

    if (pwrite(fd, buf, len, offset) < len ||
        pwrite(fd, buf + len, CHECKSUMSIZE, offset + blocklen - CHECKSUMSIZE) != CHECKSUMSIZE)
        return 1;

    return 0;
}

int encrypted_read(unsigned char key[KEYSIZE], int fd, void *buf, ssize_t len, unsigned char nonce[NONCESIZE],
                   unsigned char *additional_data, size_t addlen, size_t blocklen, uint64_t offset) {
    if (pread(fd, buf, len, offset) < len ||
        pread(fd, buf + len, CHECKSUMSIZE, offset + blocklen - CHECKSUMSIZE) != CHECKSUMSIZE)
        return 1;

    if (crypto_aead_xchacha20poly1305_ietf_decrypt_detached(buf, NULL, buf, len, buf + len, additional_data, addlen,
                                                            nonce, key) != 0)
        return 2;

    return 0;
}
