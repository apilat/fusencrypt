#ifndef FUSENCRYPT_FILESYSTEM
#define FUSENCRYPT_FILESYSTEM

#include "constants.h"

#include <stdint.h>

typedef uint64_t block_t;
typedef int64_t sblock_t;

struct fusencrypt_file_header {
    uint8_t header[3];
    uint8_t version_major;
    uint8_t version_minor;
    uint8_t version_rev;
    uint8_t reserved[10];
};

struct fusencrypt_filesystem {
    struct fusencrypt_file_header *f_header;
    struct fusencrypt_crypto *f_crypto;
    struct fusencrypt_superblock *f_superblock;

    int fd;

    uint8_t *f_buffer;
    size_t f_buffer_size;
    unsigned char f_encryption_key[KEYSIZE];
};

struct fusencrypt_crypto {
    // 0 - key, 1 - password
    uint8_t access_method;

    unsigned char argon2_salt[16];
    unsigned long long argon2_opslimit;
    size_t argon2_memlimit;
    int argon2_alg;

    unsigned char s_encryption_nonce[NONCESIZE];
};

struct fusencrypt_dir_entry {
    mode_t f_mode;

    uint64_t f_filesize;
    uint64_t f_allocated_blocks;
    block_t f_direct_block[8];
    block_t f_index_block;
    block_t f_double_index_block;
    block_t f_triple_index_block;

    uint32_t d_magic;

    char f_name[FILE_NAMELENMAX + 1];

    struct timespec f_mtime;
    struct timespec f_ctime;
};

struct fusencrypt_dir_data {
    block_t blocknum;
    unsigned entrynum;
    void *block;
    struct fusencrypt_dir_entry *entry;
};

struct fusencrypt_superblock {
    uint64_t s_block_count;
    uint64_t s_free_block_count;

    uint32_t s_magic;

    block_t s_first_data_block;
    block_t s_used_blocks_first_block;
    uint64_t s_used_blocks_block_count;
    struct fusencrypt_dir_entry s_root;

    uint8_t s_uuid[SUPERBLOCK_UUIDLEN];
    char s_label[SUPERBLOCK_LABELLENMAX + 1];
};

struct fusencrypt_directory_block {
    struct fusencrypt_dir_entry d_entries[10];
};

struct fusencrypt_index_block {
    block_t i_blocks[510];
};

int filesystem_init(struct fusencrypt_filesystem *fs, struct fusencrypt_superblock *sblock,
                    struct fusencrypt_crypto *crypto, struct fusencrypt_file_header *header);
int filesystem_superblock_init(struct fusencrypt_filesystem *fs, char *name, uint64_t size);

int filesystem_create_dir(struct fusencrypt_filesystem *fs, const char *path, mode_t mode);
int filesystem_remove_dir(struct fusencrypt_filesystem *fs, const char *path);
int filesystem_create_file(struct fusencrypt_filesystem *fs, const char *path, mode_t mode);
int filesystem_remove_file(struct fusencrypt_filesystem *fs, const char *path);

int filesystem_utimens(struct fusencrypt_filesystem *fs, const char *path, const struct timespec tv[2]);

int filesystem_truncate_file(struct fusencrypt_filesystem *fs, const char *path, off_t size);
ssize_t filesystem_read_file(struct fusencrypt_filesystem *fs, const char *path, uint8_t *buf, size_t len,
                             off_t offset);
ssize_t filesystem_write_file(struct fusencrypt_filesystem *fs, const char *path, const uint8_t *buf, size_t len,
                              off_t offset);
int filesystem_rename_file(struct fusencrypt_filesystem *fs, const char *opath, const char *npath);

struct filesystem_data_block_iterator;
struct filesystem_data_block_iterator *filesystem_foreach_data_block_new(struct fusencrypt_filesystem *fs,
                                                                         struct fusencrypt_dir_data data);
sblock_t filesystem_foreach_data_block_next(struct filesystem_data_block_iterator *iter);
void filesystem_foreach_data_block_finish(struct filesystem_data_block_iterator *iter);

void filesystem_foreach_dir_entry(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data,
                                  void (*callback)(struct fusencrypt_dir_data entry));

struct filesystem_dir_entry_iterator;
struct filesystem_dir_entry_iterator *filesystem_foreach_dir_entry_new(struct fusencrypt_filesystem *fs,
                                                                       struct fusencrypt_dir_data data);
struct fusencrypt_dir_data filesystem_foreach_dir_entry_next(struct filesystem_dir_entry_iterator *iter);
void filesystem_foreach_dir_entry_finish(struct filesystem_dir_entry_iterator *iter);

struct fusencrypt_dir_data filesystem_get_file(struct fusencrypt_filesystem *fs, const char *path);
int filesystem_fetch_file(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data *data);
void filesystem_cleanup_file(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data data);
struct fusencrypt_dir_data filesystem_get_child(struct fusencrypt_filesystem *fs, struct fusencrypt_dir_data parent,
                                                const char *name);

int filesystem_entry_is_valid(struct fusencrypt_dir_data data);
// Verifies that all loaded blocks have been unloaded after this operation.
// If this returns false, there is likely to be a memory leak.
int filesystem_check_buffer_state(struct fusencrypt_filesystem *fs);

#endif
