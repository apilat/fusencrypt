#ifndef FUSENCRYPT_DEBUG
#define FUSENCRYPT_DEBUG

#define ANSI_RED "\x1b[31m"
#define ANSI_GREEN "\x1b[32m"
#define ANSI_YELLOW "\x1b[33m"
#define ANSI_BLUE "\x1b[34m"
#define ANSI_MAGENTA "\x1b[35m"
#define ANSI_CYAN "\x1b[36m"
#define ANSI_RESET "\x1b[0m"
#define ANSI_BOLD "\x1b[1m"
#define ANSI_FAINT "\x1b[2m"

#define LEVEL_TRACE 5
#define LEVEL_DEBUG 4
#define LEVEL_INFO 3
#define LEVEL_WARN 2
#define LEVEL_ERROR 1

#define debug_print(fmt, ...)                                                                                          \
    do {                                                                                                               \
        if (debug_level() >= LEVEL_DEBUG)                                                                              \
            fprintf(stderr, ANSI_BOLD ANSI_BLUE "[%s:%d:%s] " ANSI_RESET fmt "\n", __FILE__, __LINE__, __func__,       \
                    __VA_ARGS__);                                                                                      \
    } while (0)
#define trace_print(fmt, ...)                                                                                          \
    do {                                                                                                               \
        if (debug_level() >= LEVEL_TRACE)                                                                              \
            fprintf(stderr, ANSI_FAINT "[%s:%d:%s] " ANSI_RESET fmt "\n", __FILE__, __LINE__, __func__, __VA_ARGS__);  \
    } while (0)
#define info_print(fmt, ...)                                                                                           \
    do {                                                                                                               \
        if (debug_level() >= LEVEL_INFO)                                                                               \
            fprintf(stderr, ANSI_BOLD ANSI_GREEN "[%s:%d:%s] " ANSI_RESET fmt "\n", __FILE__, __LINE__, __func__,      \
                    __VA_ARGS__);                                                                                      \
    } while (0)
#define warn_print(fmt, ...)                                                                                           \
    do {                                                                                                               \
        if (debug_level() >= LEVEL_WARN)                                                                               \
            fprintf(stderr, ANSI_BOLD ANSI_YELLOW "[%s:%d:%s] " ANSI_RESET fmt "\n", __FILE__, __LINE__, __func__,     \
                    __VA_ARGS__);                                                                                      \
    } while (0)
#define error_print(fmt, ...)                                                                                          \
    do {                                                                                                               \
        if (debug_level() >= LEVEL_ERROR)                                                                              \
            fprintf(stderr, ANSI_BOLD ANSI_RED "[%s:%d:%s] " ANSI_RESET fmt "\n", __FILE__, __LINE__, __func__,        \
                    __VA_ARGS__);                                                                                      \
    } while (0)
#define debug_tracecall()                                                                                              \
    do {                                                                                                               \
        if (debug_level() >= LEVEL_DEBUG)                                                                              \
            fprintf(stderr, ANSI_BOLD ANSI_BLUE "[%s:%d:%s]\n" ANSI_RESET, __FILE__, __LINE__, __func__);              \
    } while (0)

int debug_level();
int debug_setup();
#endif
