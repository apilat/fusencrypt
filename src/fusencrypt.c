#include "fusencrypt.h"
#include "crypto.h"
#include "debug.h"

#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static char *errmess = NULL;

int usage(char *progname) {
    printf("Usage: %s <operation> <name> [<additional options...>]\n", progname);
    puts("\tcreate <name> <size>");
    puts("\tstats <name>");
    puts("\tmount <name> <mount point> [<fuse options...>]");
    return 1;
}

void signal_handler(int signum __attribute__((unused))) {
    if (fuse_obj != NULL)
        fuse_exit(fuse_obj);
    else
        exit(1);
}

void cleanup(void) {
    if (fs != NULL)
        sodium_free(fs);
}

void establish_signal_handlers(void) {
    struct sigaction action;
    action.sa_handler = signal_handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;

    sigaction(SIGINT, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGHUP, &action, NULL);
    sigaction(SIGQUIT, &action, NULL);
}

int main(int argc, char *argv[]) {
    char *op, *name;
    int retval;

    if (debug_setup() != 0)
        return 1;
    if (sodium_init() != 0)
        return 1;

    if (argc < 3)
        return usage(argv[0]);

    atexit(cleanup);
    establish_signal_handlers();

    op = argv[1];
    name = argv[2];

    if (strcmp(op, "create") == 0) {
        if (argc != 4)
            return usage(argv[0]);
        long long size, smul;
        int slen, smulch;
        char *pwd;
        ssize_t pwdlen;

        slen = strlen(argv[3]);
        smulch = *(argv[3] + slen - 1);
        if (smulch == 'k' || smulch == 'K')
            smul = 1;
        else if (smulch == 'm' || smulch == 'M')
            smul = 1 << 10;
        else if (smulch == 'g' || smulch == 'G')
            smul = 1 << 20;
        else if (smulch == 't' || smulch == 'T')
            smul = 1 << 30;
        else
            smul = 1;

        size = atoll(argv[3]);
        size *= smul;

        fs = init_filesystem_struct();
        if (fs == NULL)
            return 1;

        pwdlen = load_password(&pwd);
        if (pwdlen > 0) {
            retval = create_filesystem(fs, name, size, pwd, pwdlen);
            clear_password(pwd, pwdlen);
        } else
            retval = 1;

        if (retval)
            goto error;

        printf("Filesystem successfully created: %s, %ld blocks\n", fs->f_superblock->s_label,
               fs->f_superblock->s_block_count);
    } else if (strcmp(op, "stats") == 0) {
        if (argc != 3)
            return usage(argv[0]);
        char *pwd;
        ssize_t pwdlen;

        fs = init_filesystem_struct();
        if (fs == NULL)
            return 1;

        pwdlen = load_password(&pwd);

        if (pwdlen > 0)
            retval = load_filesystem(fs, name, pwd, pwdlen);
        else
            retval = 1;
        if (fs->fd > 0)
            close(fs->fd);

        clear_password(pwd, pwdlen);

        if (retval)
            goto error;

        struct fusencrypt_superblock *sb = fs->f_superblock;
        printf("UUID: ");
        for (int i = 0; i < 16; i++)
            printf("%02x", sb->s_uuid[i]);
        putchar('\n');
        printf("Label: %s\n"
               "Block count: %ld, Free: %ld\n",
               sb->s_label, sb->s_block_count, sb->s_free_block_count);
    } else if (strcmp(op, "mount") == 0) {
        if (argc < 4)
            return usage(argv[0]);
        struct fuse_chan *channel;
        struct fuse_args *args;
        char *pwd;
        ssize_t pwdlen;

        fs = init_filesystem_struct();
        if (fs == NULL)
            return 1;

        pwdlen = load_password(&pwd);

        if (pwdlen > 0)
            retval = load_filesystem(fs, name, pwd, pwdlen);
        else
            retval = 1;

        clear_password(pwd, pwdlen);

        if (retval) {
            if (fs->fd > 0)
                close(fs->fd);
            goto error;
        }
        args = malloc(sizeof(*args));
        args->argc = argc - 4;
        args->argv = argv + 4;
        args->allocated = 0;
        // TODO Fix fuse mount args
        channel = fuse_mount(argv[3], args);
        if (channel == NULL) {
            free(args);
            return 1;
        }
        fuse_obj = fuse_new(channel, args, &fusencrypt_ops, sizeof(fusencrypt_ops), fs);
        free(args);

        puts("Mounting filesystem...");
        fuse_loop(fuse_obj);

        puts("Unmounting filesystem...");
        fuse_unmount(argv[3], channel);
    } else {
        puts("Incorrect operation");
        return usage(argv[0]);
    }

    return 0;
error:
    fprintf(stderr, "%s\n", errmess);
    return 1;
}

ssize_t load_password(char **pwd) {
    size_t buflen;
    ssize_t lread;

    printf("Password: ");
    fflush(stdout);

    *pwd = NULL;
    buflen = 0;

    lread = readpass(pwd, &buflen, stdin);
    putchar('\n');
    if (lread < 0)
        errmess = "Password could not be read";
    return lread;
}

void clear_password(char *pwd, size_t pwdlen) {
    memzero(pwd, pwdlen);
    free(pwd);
}

struct fusencrypt_filesystem *init_filesystem_struct() {
    struct fusencrypt_filesystem *fs;
    struct fusencrypt_superblock *sblock;
    struct fusencrypt_crypto *crypto;
    struct fusencrypt_file_header *header;
    void *ptr;

    ptr = sodium_malloc(sizeof(*fs) + sizeof(*sblock) + sizeof(*crypto) + sizeof(*header));
    if (ptr == NULL) {
        errmess = "Failed to allocate memory for filesystem";
        goto error;
    }
    fs = ptr;
    sblock = ptr + sizeof(*fs);
    crypto = ptr + sizeof(*fs) + sizeof(*sblock);
    header = ptr + sizeof(*fs) + sizeof(*sblock) + sizeof(*crypto);

    if (filesystem_init(fs, sblock, crypto, header)) {
        errmess = "Failed to initialize filesystem";
        goto error;
    }

    return fs;
error:
    sodium_free(ptr);
    return NULL;
}

int create_filesystem(struct fusencrypt_filesystem *fs, char *name, uint64_t size, char *pwd, size_t pwdlen) {
    long long blocks;

    blocks = (long long) ceil(size * 1024.0 / BLOCKSIZE);

    /* There needs to be a command line option or a detection mechanism
    char *filename;
    filename = malloc(strlen(name) + strlen(FILE_EXTENSION) + 1);
    strcpy(filename, name);
    strcat(filename, FILE_EXTENSION);
    */
    fs->fd = open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
    if (fs->fd < 0) {
        errmess = "Failed to create underlying file for filesystem";
        goto error;
    }
    ftruncate(fs->fd, blocks * BLOCKSIZE);

    if (filesystem_init_file_header(fs->f_header)) {
        errmess = "Failed to initialize file header";
        goto error;
    }

    if (filesystem_crypto_initp(fs->f_crypto, pwd, pwdlen, fs->f_encryption_key)) {
        errmess = "Failed to initialize cryptographic structures";
        goto error;
    }

    if (filesystem_superblock_init(fs, name, blocks)) {
        errmess = "Failed to initialize superblock";
        goto error;
    }

    if (filesystem_write_file_header(fs)) {
        errmess = "Failed to write file header";
        goto error;
    }
    if (filesystem_write_crypto(fs)) {
        errmess = "Failed to write cryptographic structures";
        goto error;
    }
    if (filesystem_write_superblock(fs)) {
        errmess = "Failed to write superblock";
        goto error;
    }

    fsync(fs->fd);
    close(fs->fd);

    return 0;
error:
    if (fs->fd > 0)
        close(fs->fd);
    return 1;
}

int load_filesystem(struct fusencrypt_filesystem *fs, char *name, char *pwd, size_t pwdlen) {
    int retval, openflags;
    char *filename;

    filename = malloc(strlen(name) + strlen(FILE_EXTENSION) + 1);
    strcpy(filename, name);
    openflags = O_RDWR;
    fs->fd = open(filename, openflags);
    if (fs->fd < 0)
        fs->fd = open(strcat(filename, FILE_EXTENSION), openflags);
    free(filename);
    if (fs->fd < 0) {
        errmess = "Failed to open file backing filesystem";
        goto error;
    }

    if (filesystem_load_file_header(fs)) {
        errmess = "Failed to load file header";
        goto error;
    }

    if (filesystem_load_crypto(fs)) {
        errmess = "Failed to load cryptographic structures";
        goto error;
    }
    if (filesystem_crypto_password_keygen(fs->f_crypto, pwd, pwdlen, fs->f_encryption_key)) {
        errmess = "Failed to generate key from password";
        goto error;
    }

    retval = filesystem_load_superblock(fs);
    if (retval == 1) {
        errmess = "Failed to load superblock";
        goto error;
    }
    if (retval == 2) {
        errmess = "Checksum failed on filesystem (incorrect password or file corruption)";
        goto crypt_error;
    }

    return 0;
error:
    return 1;
crypt_error:
    return 2;
}

#define GETFSP ((struct fusencrypt_filesystem *) fuse_get_context()->private_data)

// TODO Change debug to command line parameter

int fusencrypt_getattr(const char *path, struct stat *attr) {
    memset(attr, 0, sizeof(struct stat));

    struct fusencrypt_dir_data data = filesystem_get_file(GETFSP, path);
    if (filesystem_fetch_file(GETFSP, &data))
        return -errno;
    if (!filesystem_entry_is_valid(data)) {
        filesystem_cleanup_file(GETFSP, data);
        return -ENOENT;
    }
    struct fusencrypt_dir_entry *file = data.entry;

    attr->st_uid = fuse_get_context()->uid;
    attr->st_gid = fuse_get_context()->gid;

    attr->st_mode = file->f_mode;
    attr->st_nlink = (file->f_mode & S_IFDIR) ? 2 : 1;
    attr->st_size = (file->f_mode & S_IFDIR) ? file->f_allocated_blocks * BLOCKSIZE : file->f_filesize;

    attr->st_blksize = ACTUAL_BLOCKSIZE;
    attr->st_blocks = file->f_allocated_blocks * BLOCKSIZE / 512;

    attr->st_mtim = file->f_mtime;
    attr->st_ctim = file->f_ctime;

    filesystem_cleanup_file(GETFSP, data);

    filesystem_check_buffer_state(GETFSP);
    return 0;
}

int fusencrypt_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset __attribute__((unused)),
                       struct fuse_file_info *fi __attribute__((unused))) {
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);

    struct fusencrypt_dir_data dir = filesystem_get_file(GETFSP, path);
    if (filesystem_fetch_file(GETFSP, &dir))
        return -errno;
    if (!filesystem_entry_is_valid(dir)) {
        filesystem_cleanup_file(GETFSP, dir);
        return -ENOENT;
    }

    struct filesystem_dir_entry_iterator *it = filesystem_foreach_dir_entry_new(GETFSP, dir);
    struct fusencrypt_dir_data data;
    while ((data = filesystem_foreach_dir_entry_next(it)).block != NULL) {
        if (filesystem_entry_is_valid(data))
            filler(buf, data.entry->f_name, NULL, 0);
    }
    filesystem_foreach_dir_entry_finish(it);

    filesystem_cleanup_file(GETFSP, dir);

    filesystem_check_buffer_state(GETFSP);
    return 0;
}

int fusencrypt_read(const char *path, char *buf, size_t len, off_t offset,
                    struct fuse_file_info *fi __attribute__((unused))) {
    int ret = filesystem_read_file(GETFSP, path, (uint8_t *) buf, len, offset);
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_write(const char *path, const char *buf, size_t len, off_t offset,
                     struct fuse_file_info *fi __attribute__((unused))) {
    int ret = filesystem_write_file(GETFSP, path, (const uint8_t *) buf, len, offset);
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_truncate(const char *path, off_t size) {
    int ret = 0;
    if (filesystem_truncate_file(GETFSP, path, size))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_mkdir(const char *path, mode_t mode) {
    int ret = 0;
    if (filesystem_create_dir(GETFSP, path, mode))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_rmdir(const char *path) {
    int ret = 0;
    if (filesystem_remove_dir(GETFSP, path))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int ret = 0;
int fusencrypt_create(const char *path, mode_t mode, struct fuse_file_info *fi __attribute__((unused))) {
    if (filesystem_create_file(GETFSP, path, mode))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_unlink(const char *path) {
    int ret = 0;
    if (filesystem_remove_file(GETFSP, path))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_utimens(const char *path, const struct timespec tv[2]) {
    int ret = 0;
    if (filesystem_utimens(GETFSP, path, tv))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_rename(const char *opath, const char *npath) {
    int ret = 0;
    if (filesystem_rename_file(GETFSP, opath, npath))
        ret = -errno;
    filesystem_check_buffer_state(GETFSP);
    return ret;
}

int fusencrypt_not_supported() { return -ENOTSUP; }
