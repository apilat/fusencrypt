#ifndef FUSENCRYPT_CONSTANTS
#define FUSENCRYPT_CONSTANTS

#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_REV 1

#define FILE_HEADER ((const char *) "FEF")
#define FILE_EXTENSION ((const char *) ".fef")

#define BLOCKSIZE 4096UL
#define FSBUFFERSIZE 10485760UL

#define CRYPTO_LEN 256U

#define SUPERBLOCK_MAGIC 0xfe4c5b17
#define SUPERBLOCK_UUIDLEN 16U
#define SUPERBLOCK_LABELLENMAX 63U

#define FILE_NAMELENMAX 255U
#define DIRECTORY_MAGIC 0xe4c7d170
#define FILE_MAGIC 0xe4c7f17e
#define TEMP_MAGIC 0xe4c77e37

#include <sodium.h>

#define KEYSIZE crypto_aead_xchacha20poly1305_ietf_KEYBYTES
#define NONCESIZE crypto_aead_xchacha20poly1305_ietf_NPUBBYTES
#define CHECKSUMSIZE crypto_aead_xchacha20poly1305_ietf_ABYTES

#define ACTUAL_BLOCKSIZE (BLOCKSIZE - CHECKSUMSIZE)

#ifdef DEBUG
#define ARGON2_OPSLIMIT crypto_pwhash_argon2i_OPSLIMIT_INTERACTIVE
#define ARGON2_MEMLIMIT crypto_pwhash_argon2i_MEMLIMIT_INTERACTIVE
#else
#define ARGON2_OPSLIMIT crypto_pwhash_argon2i_OPSLIMIT_SENSITIVE
#define ARGON2_MEMLIMIT crypto_pwhash_argon2i_MEMLIMIT_SENSITIVE
#endif
#define ARGON2_ALG crypto_pwhash_argon2i_ALG_ARGON2I13

#endif
