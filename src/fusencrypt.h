#ifndef FUSENCRYPT
#define FUSENCRYPT

#define FUSE_USE_VERSION 28
#define _FILE_OFFSET_BITS 64
#include <fuse.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "filesystem.h"

int fusencrypt_getattr(const char *path, struct stat *attr);

int fusencrypt_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
int fusencrypt_mkdir(const char *path, mode_t mode);
int fusencrypt_rmdir(const char *path);

int fusencrypt_create(const char *path, mode_t mode, struct fuse_file_info *fi);
int fusencrypt_unlink(const char *path);

int fusencrypt_truncate(const char *path, off_t size);
int fusencrypt_read(const char *path, char *buf, size_t len, off_t offset, struct fuse_file_info *fi);
int fusencrypt_write(const char *path, const char *buf, size_t len, off_t offset, struct fuse_file_info *fi);
int fusencrypt_rename(const char *opath, const char *npath);

int fusencrypt_utimens(const char *path, const struct timespec tv[2]);

int fusencrypt_not_supported();


struct fusencrypt_filesystem *init_filesystem_struct();

int create_filesystem(struct fusencrypt_filesystem *fs, char *name, uint64_t size, char *pwd, size_t pwdlen);
int load_filesystem(struct fusencrypt_filesystem *fs, char *name, char *pwd, size_t pwdlen);


ssize_t load_password(char **pwd);
void clear_password(char *pwd, size_t pwdlen);

void establish_signal_handlers(void);
void cleanup(void);
void signal_handler(int signum);

struct fuse_operations fusencrypt_ops = {
    .getattr = fusencrypt_getattr,
    .readlink = fusencrypt_not_supported,
    .mkdir = fusencrypt_mkdir,
    .unlink = fusencrypt_unlink,
    .rmdir = fusencrypt_rmdir,
    .symlink = fusencrypt_not_supported,
    .rename = fusencrypt_rename,
    .link = fusencrypt_not_supported,
    .chmod = fusencrypt_not_supported,
    .chown = fusencrypt_not_supported,
    .truncate = fusencrypt_truncate,
    .read = fusencrypt_read,
    .write = fusencrypt_write,
    //.statfs = fusencrypt_statfs,
    .readdir = fusencrypt_readdir,
    //.access = fusencrypt_access,
    .create = fusencrypt_create,
    .utimens = fusencrypt_utimens,
};

struct fusencrypt_filesystem *fs;
struct fuse *fuse_obj;

#endif
