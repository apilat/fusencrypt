import os
import os.path as path
import pytest
import random

from utils import random_name, random_buffer

def test_rw_simple(filesystem):
    filn = path.join(filesystem.dir, random_name())
    fd = os.open(filn, os.O_CREAT | os.O_EXCL | os.O_RDWR, 600)

    write = random_buffer(131072)
    os.write(fd, write)
    os.lseek(fd, 0, os.SEEK_SET)
    read = os.read(fd, 131072)

    assert write == read

    os.close(fd)
    os.remove(filn)

def test_rw_offset(filesystem):
    filn = path.join(filesystem.dir, random_name())
    fd = os.open(filn, os.O_CREAT | os.O_EXCL | os.O_RDWR, 600)

    offset1 = random.randint(0, 32768)
    offset2 = random.randint(0, 32768)
    write = random_buffer(131072)
    os.pwrite(fd, write, offset1)
    read = os.pread(fd, 65536, offset1 + offset2)

    assert write[offset2:offset2+65536] == read

    os.close(fd)
    os.remove(filn)

def test_rw_repeated(filesystem):
    filn = path.join(filesystem.dir, random_name())
    fd = os.open(filn, os.O_CREAT | os.O_EXCL | os.O_RDWR, 600)

    write = random_buffer(131072)
    os.write(fd, write)
    os.lseek(fd, 0, os.SEEK_SET)

    for i in range(131072 // 16384):
        read = os.read(fd, 16384)

        assert write[i*16384:(i+1)*16384] == read

    os.close(fd)
    os.remove(filn)

def test_rw_repeated_offset(filesystem):
    filn = path.join(filesystem.dir, random_name())
    fd = os.open(filn, os.O_CREAT | os.O_EXCL | os.O_RDWR, 600)

    write = random_buffer(131072)
    os.pwrite(fd, write, 0)

    for _ in range(10):
        offset = random.randint(0, 65536)
        read = os.pread(fd, 65536, offset)

        assert write[offset:offset+65536] == read

    os.close(fd)
    os.remove(filn)

def test_rw_large(filesystem):
    filn = path.join(filesystem.dir, random_name())
    fd = os.open(filn, os.O_CREAT | os.O_EXCL | os.O_RDWR, 600)

    data = random_buffer(16777216)
    os.write(fd, data)
    os.lseek(fd, 0, os.SEEK_SET)
    read = os.read(fd, 16777216)

    assert data == read

    os.close(fd)
    os.remove(filn)

def test_readonly(filesystem):
    filn = path.join(filesystem.dir, random_name())
    fd = os.open(filn, os.O_CREAT | os.O_EXCL | os.O_RDONLY, 600)

    with pytest.raises(OSError):
        os.write(fd, b"\x01\x02\x03\x04")

    os.close(fd)
    os.remove(filn)
