import os
import os.path as path
import pytest

from utils import random_name

def test_file_single(filesystem):
    d = filesystem.dir
    assert os.listdir(d) == []
    for _ in range(10):
        filen = random_name()
        filepath = path.join(d, filen)
        os.close(os.open(filepath, os.O_CREAT))
        assert os.listdir(d) == [filen]
        os.remove(filepath)
        assert os.listdir(d) == []

def test_file_multi(filesystem):
    d = filesystem.dir
    files = [random_name() for _ in range(10)]
    for cur in files:
        os.close(os.open(path.join(d, cur), os.O_CREAT))
    assert os.listdir(d) == files
    for cur in files:
        os.remove(path.join(d, cur))
    assert os.listdir(d) == []

def test_file_duplicatefile(filesystem):
    p = path.join(filesystem.dir, random_name())
    os.close(os.open(p, os.O_CREAT))
    with pytest.raises(FileExistsError):
        os.open(p, os.O_CREAT | os.O_EXCL)
    os.remove(p)

def test_file_duplicatedir(filesystem):
    p = path.join(filesystem.dir, random_name())
    os.mkdir(p)
    with pytest.raises(FileExistsError):
        os.open(p, os.O_CREAT | os.O_EXCL)
    os.rmdir(p)

def test_file_nested(filesystem):
    dirn = random_name()
    p = path.join(filesystem.dir, dirn)
    filen = random_name()
    f = path.join(p, filen)
    os.mkdir(p)
    os.close(os.open(f, os.O_CREAT))
    assert os.listdir(filesystem.dir) == [dirn]
    assert os.listdir(p) == [filen]
    os.remove(f)
    os.rmdir(p)

def test_file_notexists(filesystem):
    with pytest.raises(FileNotFoundError):
        os.open(path.join(filesystem.dir, "a", "b"), os.O_CREAT)

def test_file_parentnotdir(filesystem):
    p = path.join(filesystem.dir, random_name())
    os.close(os.open(p, os.O_CREAT | os.O_RDONLY))
    with pytest.raises(NotADirectoryError):
        os.open(path.join(p, random_name()), os.O_CREAT)
    os.remove(p)
