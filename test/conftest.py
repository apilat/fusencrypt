import os
import tempfile
import subprocess
import pytest
from collections import namedtuple

Filesystem = namedtuple("Filesystem", ["dir", "file", "proc"])

@pytest.fixture(scope="session")
def filesystem():
    dir = tempfile.mkdtemp(prefix="/tmp/test_fusencrypt_")
    file = tempfile.mktemp(prefix="/tmp/test_fusencrypt_fs_")

    proc_create = subprocess.Popen(["../bin/fusencrypt", "create", file, "100M"],
            stdin=subprocess.PIPE)
    proc_create.communicate(b"pass1234\n", timeout=5)
    proc_create.wait()
    assert proc_create.returncode == 0

    proc = subprocess.Popen(["../bin/fusencrypt", "mount", file, dir],
                            stdin=subprocess.PIPE)
    try:
        proc.communicate(b"pass1234\n", timeout=5)
    except subprocess.TimeoutExpired:
        pass

    yield Filesystem(dir, file, proc)

    proc.terminate()
    proc.wait()
    os.remove(file)
    os.rmdir(dir)
