import random
import string


def random_name():
    return ''.join(random.choices(string.ascii_letters, k=16))

def random_buffer(size):
    buf = bytearray(size)
    for i in range(size):
        buf[i] = random.randint(0, 255)
    return buf
