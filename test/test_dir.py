import os
import os.path as path
import pytest

from utils import random_name

def test_readdir(filesystem):
    d = filesystem.dir
    assert os.listdir(d) == []

    _dirn = random_name()
    _filen = random_name()
    os.mkdir(path.join(d, _dirn))
    os.close(os.open(path.join(d, _filen), os.O_CREAT))
    assert os.listdir(d) == [_dirn, _filen]

    os.rmdir(path.join(d, _dirn))
    os.remove(path.join(d, _filen))
    assert os.listdir(d) == []

def test_dir_single(filesystem):
    d = filesystem.dir
    assert os.listdir(d) == []
    for _ in range(10):
        dirn = random_name()
        dirpath = path.join(d, dirn)
        os.mkdir(dirpath)
        assert os.listdir(d) == [dirn]
        os.rmdir(dirpath)
        assert os.listdir(d) == []

def test_dir_multi(filesystem):
    d = filesystem.dir
    dirs = [random_name() for _ in range(10)]
    for cur in dirs:
        os.mkdir(path.join(d, cur))
    assert os.listdir(d) == dirs
    for cur in dirs:
        os.rmdir(path.join(d, cur))
    assert os.listdir(d) == []

def test_dir_duplicate(filesystem):
    p = path.join(filesystem.dir, random_name())
    os.mkdir(p)
    with pytest.raises(FileExistsError):
        os.mkdir(p)
    os.rmdir(p)

def test_dir_nested(filesystem):
    d = filesystem.dir
    os.mkdir(path.join(d, "a"))
    os.mkdir(path.join(d, "a", "b"))
    os.mkdir(path.join(d, "a", "c"))
    os.mkdir(path.join(d, "a", "c", "d"))
    assert os.listdir(d) == ["a"]
    assert os.listdir(path.join(d, "a")) == ["b", "c"]
    assert os.listdir(path.join(d, "a", "b")) == []
    assert os.listdir(path.join(d, "a", "c")) == ["d"]
    assert os.listdir(path.join(d, "a", "c", "d")) == []
    os.rmdir(path.join(d, "a", "c", "d"))
    os.rmdir(path.join(d, "a", "c"))
    os.rmdir(path.join(d, "a", "b"))
    os.rmdir(path.join(d, "a"))

def test_dir_notexists(filesystem):
    with pytest.raises(FileNotFoundError):
        os.mkdir(path.join(filesystem.dir, "a", "b"))

def test_dir_parentnotdir(filesystem):
    p = path.join(filesystem.dir, random_name())
    os.close(os.open(p, os.O_CREAT | os.O_RDONLY))
    with pytest.raises(NotADirectoryError):
        os.mkdir(path.join(p, random_name()))
    os.remove(p)
