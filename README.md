# fusencrypt - Securely encrypted filesystem built using FUSE and libsodium

## Disclamer: I am neither a professional programmer or a cryptographer, so it is quite likely that this code contains horrible security bugs. It is only intended as a learning exercise for me and should not be used for storing valuable data.

## Architectural design
### Filesystem

* The whole filesystem is stored in a **single file** on disk, which makes it portable and provides no information to the attacker about the contents inside.
* The filesystem is split into **blocks of 4096 bytes** which are the smallest allocatable data unit.
* The layout of an examplar 256 MiB filesystem is as follows:

>>>
0x00000000 Filesystem superblock  
0x00001000 Free block bitmap  
0x00004000 First block of actual data  
      ...  
0x09999000 Last block of actual data  
0x10000000 Filesystem superblock (redundant copy)  
>>>

* Metadata file information is stored in directory entries (therefore hardlinks are not supported).
* Each file (including directories) is allocated an index block which stores the blocks in which the file is located. The index block for / is the 3rd block in the filesystem.

### Encryption

* The filesystem is protected by a **256-bit encryption key** which can be supplied by the creator or generated from a password using **Argon2**.
* All data in the filesystem is encrypted and authenticated using **libsodium AEAD** with the **XChaCha20-Poly1305** algorithm at the block level.
